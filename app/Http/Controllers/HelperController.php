<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Mail\WelcomeMail;

class HelperController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Sends mail as soon as user registers with the application
     *
     * @param [String] $toEmail
     * @param [Object] $mailBody
     * @return void
     */
    public function sendWelcomeEmail($toEmail, $mailBody)
    {
      return  Mail::to($toEmail)->send(new WelcomeMail($mailBody));
    }
}
