<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>


## About Project

This is a simple web application build with Laraval, offers basic features  

- A basic User authentication for Login, registration.
- Facebook login using Laraval socialite package.
- Once loggedIn user can view previous posts or post a new note.

## Installation 

- Clone the repository to new folder
- Open terminal and navigate to the folder. 
- RUN composer install
- Rename .env.example to .env and add valid MySql database information. 
- Change the FB and twitter keys,callbacks to valid onces in the .env file.  
- RUN artisan serve to see the project running. 

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
