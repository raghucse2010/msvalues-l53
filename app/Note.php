<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
    /**
     * Mention the required columns to be filled while mass assignment.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description'
    ];

}
