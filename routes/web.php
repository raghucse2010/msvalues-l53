<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route::get('/home', function () {
//     return "Hello World";
// });

Route::resource('Note', 'NoteController');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('auth/{provider}', 'Auth\LoginController@redirectToProvider');
Route::get('auth/{provider}/callback', 'Auth\LoginController@handleProviderCallback');


Route::get('/sendemail', function () {
    
        $data = array(
            'name' => "Budingi.com",
        );
    
        Mail::send('email.demomail', $data, function ($message) {
    
            $message->from('dongri2dubai@gmail.com', 'Welcome Mail Laravel');
    
            $message->to('raghunandan.vrn@gmail.com')->subject('Welcome to Laravel test email');
    
        });
    
        return "Your email has been sent successfully";



        // Mail::send(['text'=>'mail'], $data, function($message) {
        //     $message->to('abc@gmail.com', 'Tutorials Point')->subject
        //        ('Laravel Basic Testing Mail');
        //     $message->from('xyz@gmail.com','Virat Gandhi');
        //  });
    
    });
